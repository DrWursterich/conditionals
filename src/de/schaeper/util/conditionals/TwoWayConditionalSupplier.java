package de.schaeper.util.conditionals;

import java.util.Optional;
import java.util.function.Supplier;

public class TwoWayConditionalSupplier<T>
		extends TwoWayConditionalValueHolder<T>
		implements Supplier<Optional<T>> {

	public static <T> Builder<T> build(
			final Class<T> clazz,
			final boolean first,
			final boolean second) {
		return new TwoWayConditionalSupplier.Builder<>(first, second);
	}

	public static class Builder<V>
			extends TwoWayConditionalValueHolder.Builder<
					TwoWayConditionalSupplier<V>,
					V>
			implements Supplier<Optional<V>> {

		private Builder(
				final boolean first,
				final boolean second) {
			super(first, second);
		}

		@Override
		public Optional<V> get() {
			return this.getConditional().get();
		}

		@Override
		protected TwoWayConditionalSupplier<V> createConditional(
				final boolean first,
				final boolean second) {
			return new TwoWayConditionalSupplier<>(first, false);
		}
	}

	private TwoWayConditionalSupplier(
			final boolean first,
			final boolean second) {
		super(first, second);
	}

	@Override
	public Optional<T> get() {
		return Optional.ofNullable(this.getResult());
	}
}
