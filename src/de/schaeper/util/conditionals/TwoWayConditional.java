package de.schaeper.util.conditionals;

public class TwoWayConditional {
	private static final byte NEITHER = 0;
	private static final byte FIRST = 1;
	private static final byte SECOND = 2;
	private static final byte BOTH = FIRST | SECOND;

	protected final byte value;

	public TwoWayConditional(
			final boolean first,
			final boolean second) {
		this.value = (byte)((first ? FIRST : 0) | (second ? SECOND : 0));
	}

	public boolean isBoth() {
		return this.value == BOTH;
	}

	public boolean isAtLeastOne() {
		return this.value != NEITHER;
	}

	public boolean isExctlyOne() {
		return (this.value + 1) >> 1 == 1;
	}

	public boolean isFirst() {
		return (this.value & FIRST) == this.value; 
	}

	public boolean isOnlyFirst() {
		return (this.value & FIRST) == FIRST; 
	}

	public boolean isSecond() {
		return (this.value & SECOND) == this.value; 
	}

	public boolean isOnlySecond() {
		return (this.value & SECOND) == SECOND; 
	}

	public boolean isNeither() {
		return this.value == NEITHER;
	}
}
