package de.schaeper.util.conditionals;

public class TwoWayConditionalRunnable
		extends TwoWayConditionalValueHolder<Runnable>
		implements Runnable {

	public static Builder build(final boolean first, final boolean second) {
		return new TwoWayConditionalRunnable.Builder(first, second);
	}

	public static class Builder
			extends TwoWayConditionalValueHolder.Builder<
					TwoWayConditionalRunnable,
					Runnable>
			implements Runnable {

		private Builder(
				final boolean first,
				final boolean second) {
			super(first, second);
		}

		public Builder ifAny(final Runnable value) {
			return this.getClass().cast(super.ifAny(value));
		}

		@Override
		public void run() {
			this.getConditional().run();
		}

		@Override
		protected TwoWayConditionalRunnable createConditional(
				boolean first,
				boolean second) {
			return new TwoWayConditionalRunnable(first, second);
		}
	}

	public TwoWayConditionalRunnable(
			final boolean first,
			final boolean second) {
		super(first, second);
	}

	@Override
	public void run() {
		if (this.getResult() != null) {
			this.getResult().run();
		}
	}
}
