package de.schaeper.util.conditionals;

import java.util.Arrays;


public class MultiConditional {
	private final long value;
	private boolean hasBeenRun;

	public static MultiConditional conditions(final Boolean...conditions) {
		return new MultiConditional(conditions);
	}

	private MultiConditional(final Boolean...conditions) {
		if (conditions.length == 0) {
			throw new IllegalArgumentException("Need atleast one Condition.");
		}
		int value = 0;
		for (int i = conditions.length - 1; i >= 0; i--) {
			value = (value << 1) | (conditions[i] ? 1 : 0);
		}
		this.value = value;
	}

	public MultiConditional runIfThese(
			final Runnable runnable,
			final int...conditions) {
		if (!this.hasBeenRun
				&& Arrays.stream(conditions)
					.allMatch(e -> ((this.value >> (e - 1)) & 1) == 1)) {
			runnable.run();
			this.hasBeenRun = true;
		}
		return this;
	}

	public boolean hasBeenRun() {
		return this.hasBeenRun;
	}
}
