package de.schaeper.util.conditionals;

class TwoWayConditionalValueHolder<T> extends TwoWayConditional {
	private boolean resultHasBeenDetermined;
	private T result;

	public static abstract class Builder<
				T extends TwoWayConditionalValueHolder<V>,
				V> {
		private final T conditional;

		protected Builder(
				final boolean first,
				final boolean second) {
			this.conditional = this.createConditional(first, second);
		}

		protected abstract T createConditional(
				final boolean first,
				final boolean second);

		public Builder<T, V> ifAny(final V value) {
			this.conditional.ifAny(value);
			return this;
		}
		
		public Builder<T, V> ifAtleastOne(final V value) {
			this.conditional.ifAtleastOne(value);
			return this;
		}

		public Builder<T, V> ifExactlyOne(final V value) {
			this.conditional.ifExactlyOne(value);
			return this;
		}

		public Builder<T, V> ifBoth(final V value) {
			this.conditional.ifBoth(value);
			return this;
		}

		public Builder<T, V> ifFirst(final V value) {
			this.conditional.ifFirst(value);
			return this;
		}

		public Builder<T, V> ifOnlyFirst(final V value) {
			this.conditional.ifOnlyFirst(value);
			return this;
		}

		public Builder<T, V> ifSecond(final V value) {
			this.conditional.ifSecond(value);
			return this;
		}

		public Builder<T, V> ifOnlySecond(final V value) {
			this.conditional.ifOnlySecond(value);
			return this;
		}

		public Builder<T, V> ifNeither(final V value) {
			this.conditional.ifNeither(value);
			return this;
		}

		public T getConditional() {
			return this.conditional;
		}
	}

	protected TwoWayConditionalValueHolder(
			final boolean first,
			final boolean second) {
		super(first, second);
		this.resultHasBeenDetermined = false;
	}

	public void ifAny(final T value) {
		this.setResultIfNotAlreadySet(value);
	}

	public void ifAtleastOne(final T value) {
		if (this.isAtLeastOne()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifExactlyOne(final T value) {
		if (this.isExctlyOne()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifBoth(final T value) {
		if (this.isBoth()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifFirst(final T value) {
		if (this.isFirst()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifOnlyFirst(final T value) {
		if (this.isOnlyFirst()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifSecond(final T value) {
		if (this.isSecond()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifOnlySecond(final T value) {
		if (this.isOnlySecond()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	public void ifNeither(final T value) {
		if (this.isNeither()) {
			this.setResultIfNotAlreadySet(value);
		}
	}

	protected T getResult() {
		return this.result;
	}

	private void setResultIfNotAlreadySet(final T result) {
		if (!this.resultHasBeenDetermined) {
			this.result = result;
			this.resultHasBeenDetermined = true;
		}
	}
}
