# Conditionals

Conditionals is a utility-library to evaluate complex conditions structured, intuitivly and inline.  
It allows to use conditions, that are beyond the scope of `a ? b : c` inside of functions or streams and will improve code-readability in general.

## Installing

### Using JAM

Add the following lines to your `jam.ini`:

```ini
[conditionals]
url=https://gitlab.com/DrWursterich/conditionals
version=1.0.0
```

You can then run `jam build` as usual.
For more details see [jam](https://gitlab.com/DrWursterich/jam).

### Manually

```bash
git clone https://gitlab.com/DrWursterich/conditionals.git
cd conditionals
javac $(find src -name *.java)
jar -cvf conditionals.jar src
mv conditionals.jar /my/project/lib/
```

## Examples

### Normally

```java
if (firstCondition) {
	if (secondCondition) {
		System.out.println("Both Conditions are true");
	} else {
		System.out.println("Only the first Condition is true");
	}
} else {
	if (secondCondition) {
		System.out.println("Only the second Condition is true");
	} else {
		System.out.println("Neither Condition is true");
	}
}
```

### With Conditionals

```java
TwoWayConditionalRunnable.build(firstCondition, secondCondition)
		.ifBoth(() -> System.out.println("Both Conditions are true"))
		.ifFirstOnly(() -> System.out.println("Only the first Condition is true"))
		.ifSecondOnly(() -> System.out.println("Only the second Condition is true"))
		.ifNeither(() -> System.out.println("Neither Condition is true"))
		.getConditional()
		.run();
```

or even

```java
TwoWayConditionalSupplier.build(
			String.class,
			firstCondition,
			secondCondition)
		.ifBoth("Both Conditions are true")
		.ifFirstOnly("Only the first Condition is true")
		.ifSecondOnly("Only the second Condition is true")
		.ifNeither("Neither Condition is true")
		.getConditional()
		.get()
		.ifPresent(System.out::print);
```

